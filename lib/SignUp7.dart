import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './SignUp5.dart';
import 'package:adobe_xd/page_link.dart';

class SignUp7 extends StatelessWidget {
  SignUp7({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafafa),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(219.0, 328.0),
            child:
                // Adobe XD layer: 'Camera' (group)
                SizedBox(
              width: 25.0,
              height: 25.0,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 25.0, 25.0),
                    size: Size(25.0, 25.0),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(149.0, 20.0),
            child: Text(
              'Vendor Profile',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 25,
                color: const Color(0xff040202),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(216.1, 67.0),
            child: SizedBox(
              width: 174.0,
              child: Text(
                'Use Personal Details',
                style: TextStyle(
                  fontFamily: 'Arial',
                  fontSize: 17,
                  color: const Color(0xff000000),
                  height: 1.7647058823529411,
                ),
                textHeightBehavior:
                    TextHeightBehavior(applyHeightToFirstAscent: false),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(),
        ],
      ),
    );
  }
}
