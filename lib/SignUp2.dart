import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './SignUp4.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignUp2 extends StatelessWidget {
  SignUp2({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafafa),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(84.0, 116.0),
            child: Text(
              'Full Name (As appears on ID)',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 14,
                color: const Color(0x40040202),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(84.0, 199.0),
            child: Text(
              'ID Number',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 14,
                color: const Color(0x40040202),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(184.0, 746.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  ease: Curves.easeOut,
                  duration: 0.3,
                  pageBuilder: () => SignUp4(),
                ),
              ],
              child: SizedBox(
                width: 87.0,
                height: 87.0,
                child: Stack(
                  children: <Widget>[
                    Pinned.fromSize(
                      bounds: Rect.fromLTWH(0.0, 0.0, 87.0, 87.0),
                      size: Size(87.0, 87.0),
                      pinLeft: true,
                      pinRight: true,
                      pinTop: true,
                      pinBottom: true,
                      child:
                          // Adobe XD layer: 'Add' (group)
                          Stack(
                        children: <Widget>[
                          Pinned.fromSize(
                            bounds: Rect.fromLTWH(0.0, 0.0, 87.0, 87.0),
                            size: Size(87.0, 87.0),
                            pinLeft: true,
                            pinRight: true,
                            pinTop: true,
                            pinBottom: true,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.elliptical(9999.0, 9999.0)),
                                color: const Color(0xff1fc79c),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(),
                  ],
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(64.0, 314.0),
            child: SizedBox(
              width: 126.0,
              height: 122.0,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 122.0, 120.0),
                    size: Size(126.4, 121.6),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(89.2, 85.6, 37.2, 36.0),
                    size: Size(126.4, 121.6),
                    pinRight: true,
                    pinBottom: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 37.2, 36.0),
                          size: Size(37.2, 36.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_qmwk02,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(9.8, 10.4, 18.8, 14.4),
                          size: Size(37.2, 36.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_t1ukin,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(64.0, 491.0),
            child: SizedBox(
              width: 126.0,
              height: 122.0,
              child: Stack(
                children: <Widget>[
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(0.0, 0.0, 122.0, 120.0),
                    size: Size(126.4, 121.6),
                    pinLeft: true,
                    pinRight: true,
                    pinTop: true,
                    pinBottom: true,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                        color: const Color(0xffffffff),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Pinned.fromSize(
                    bounds: Rect.fromLTWH(89.2, 85.6, 37.2, 36.0),
                    size: Size(126.4, 121.6),
                    pinRight: true,
                    pinBottom: true,
                    fixedWidth: true,
                    fixedHeight: true,
                    child: Stack(
                      children: <Widget>[
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(0.0, 0.0, 37.2, 36.0),
                          size: Size(37.2, 36.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_qmwk02,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Pinned.fromSize(
                          bounds: Rect.fromLTWH(9.8, 10.4, 18.8, 14.4),
                          size: Size(37.2, 36.0),
                          pinLeft: true,
                          pinRight: true,
                          pinTop: true,
                          pinBottom: true,
                          child: SvgPicture.string(
                            _svg_t1ukin,
                            allowDrawingOutsideViewBox: true,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(228.0, 364.0),
            child: Text(
              'Take A Photo Of Your\nID',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 20,
                color: const Color(0xff040202),
                height: 1.5,
              ),
              textHeightBehavior:
                  TextHeightBehavior(applyHeightToFirstAscent: false),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(230.0, 541.0),
            child: Text(
              'Take A Photo Of Your\nFace',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 20,
                color: const Color(0xff040202),
                height: 1.5,
              ),
              textHeightBehavior:
                  TextHeightBehavior(applyHeightToFirstAscent: false),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(84.5, 142.5),
            child: SvgPicture.string(
              _svg_eqx8ew,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(137.0, 20.0),
            child: Text(
              'User Verification',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 25,
                color: const Color(0xff040202),
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_qmwk02 =
    '<svg viewBox="195.2 151.6 37.2 36.0" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path transform="matrix(0.999848, -0.017452, 0.017452, 0.999848, 195.19, 152.22)" d="M 18.3076229095459 0 C 28.41864395141602 0 36.6152458190918 7.922898769378662 36.6152458190918 17.6962890625 C 36.6152458190918 27.46967697143555 28.41864395141602 35.392578125 18.3076229095459 35.392578125 C 8.196602821350098 35.392578125 0 27.46967697143555 0 17.6962890625 C 0 7.922898769378662 8.196602821350098 0 18.3076229095459 0 Z" fill="#00ffc1" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
const String _svg_t1ukin =
    '<svg viewBox="205.0 162.0 18.8 14.4" ><path transform="translate(205.0, 162.0)" d="M 9.4111328125 9.625 C 10.70516300201416 9.625 11.76391506195068 8.758749961853027 11.76391506195068 7.699999809265137 C 11.76391506195068 6.641249656677246 10.70516300201416 5.774999618530273 9.4111328125 5.774999618530273 C 8.11710262298584 5.774999618530273 7.058349609375 6.641249656677246 7.058349609375 7.699999809265137 C 7.058349609375 8.758749961853027 8.11710262298584 9.625 9.4111328125 9.625 Z M 9.4111328125 3.849999904632568 C 11.99919319152832 3.849999904632568 14.11669921875 5.582499504089355 14.11669921875 7.699999809265137 C 14.11669921875 9.817499160766602 11.99919319152832 11.54999923706055 9.4111328125 11.54999923706055 C 6.823071479797363 11.54999923706055 4.70556640625 9.817499160766602 4.70556640625 7.699999809265137 C 4.70556640625 5.582499504089355 6.823071479797363 3.849999904632568 9.4111328125 3.849999904632568 Z M 16.469482421875 14.43749904632568 L 2.352783203125 14.43749904632568 C 1.76458740234375 14.43749904632568 1.1763916015625 14.24499988555908 0.7058349251747131 13.85999965667725 C 0.2352783232927322 13.47499942779541 0 12.99374961853027 0 12.51249885559082 L 0 3.849999904632568 C 0 2.791249752044678 1.058752417564392 1.924999952316284 2.352783203125 1.924999952316284 L 4.235009670257568 1.924999952316284 L 5.99959659576416 0.481249988079071 C 6.352514743804932 0.1925000101327896 6.823071479797363 0 7.411267280578613 0 L 11.52863788604736 0 C 11.99919319152832 0 12.46975135803223 0.1925000101327896 12.82266902923584 0.481249988079071 L 14.58725547790527 1.924999952316284 L 16.469482421875 1.924999952316284 C 17.76351165771484 1.924999952316284 18.822265625 2.791249752044678 18.822265625 3.849999904632568 L 18.822265625 12.51249885559082 C 18.822265625 13.57124900817871 17.76351165771484 14.43749904632568 16.469482421875 14.43749904632568 Z" fill="#fafafa" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_eqx8ew =
    '<svg viewBox="84.5 142.5 279.0 81.0" ><path transform="translate(84.5, 223.5)" d="M 0 0 L 279 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(84.5, 142.5)" d="M 0 0 L 279 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
