import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './SignUp2.dart';
import 'package:adobe_xd/page_link.dart';

class SignUp1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafafa),
      body: Stack(
        children: <Widget>[
          Container(),
          Container(),
          Container(),
          Pinned.fromPins(
            Pin(size: 183.0, middle: 0.5),
            Pin(size: 28.0, start: 20.0),
            child: Text(
              'Type Of Account',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 25,
                color: const Color(0xff040202),
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}
