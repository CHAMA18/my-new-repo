import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './SignUp8.dart';
import 'package:adobe_xd/page_link.dart';
import './SignUp6.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignUp9 extends StatelessWidget {
  SignUp9({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfffafafa),
      body: Stack(
        children: <Widget>[
          Pinned.fromPins(
            Pin(size: 25.0, middle: 0.5093),
            Pin(size: 25.0, middle: 0.3766),
            child:
                // Adobe XD layer: 'Camera' (group)
                Stack(
              children: <Widget>[
                Pinned.fromPins(
                  Pin(start: 0.0, end: 0.0),
                  Pin(start: 0.0, end: 0.0),
                  child: Container(
                    decoration: BoxDecoration(),
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromPins(
            Pin(size: 168.0, middle: 0.5017),
            Pin(size: 28.0, start: 20.0),
            child: Text(
              'Delivery Profile',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 25,
                color: const Color(0xff040202),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Container(),
          Pinned.fromPins(
            Pin(size: 158.0, middle: 0.7542),
            Pin(size: 19.0, start: 67.0),
            child: Text(
              'Use Personal Details',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 17,
                color: const Color(0xff000000),
                height: 1.7647058823529411,
              ),
              textHeightBehavior:
                  TextHeightBehavior(applyHeightToFirstAscent: false),
              textAlign: TextAlign.center,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 146.0, middle: 0.7638),
            Pin(size: 19.0, start: 101.0),
            child: Text(
              'Use Vendor Details',
              style: TextStyle(
                fontFamily: 'Arial',
                fontSize: 17,
                color: const Color(0xff000000),
                height: 1.7647058823529411,
              ),
              textHeightBehavior:
                  TextHeightBehavior(applyHeightToFirstAscent: false),
              textAlign: TextAlign.center,
            ),
          ),
          Container(),
          Pinned.fromPins(
            Pin(size: 394.0, middle: 0.3197),
            Pin(start: 131.0, end: 0.0),
            child: SingleChildScrollView(
              child: SizedBox(
                width: 394.0,
                height: 1020.0,
                child: Stack(
                  children: <Widget>[
                    Pinned.fromPins(
                      Pin(size: 218.0, middle: 0.5653),
                      Pin(size: 218.0, start: 6.0),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(start: 0.0, end: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.elliptical(9999.0, 9999.0)),
                                color: const Color(0xffffffff),
                                boxShadow: [
                                  BoxShadow(
                                    color: const Color(0x29000000),
                                    offset: Offset(0, 3),
                                    blurRadius: 6,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 52.3, end: 0.9),
                            Pin(size: 50.4, end: 3.6),
                            child: Stack(
                              children: <Widget>[
                                Pinned.fromPins(
                                  Pin(start: 0.0, end: 0.0),
                                  Pin(start: 0.0, end: 0.0),
                                  child: SvgPicture.string(
                                    _svg_ns6ele,
                                    allowDrawingOutsideViewBox: true,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Pinned.fromPins(
                                  Pin(start: 9.8, end: 8.6),
                                  Pin(start: 10.4, end: 11.2),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 65.0, middle: 0.348),
                      Pin(size: 16.0, middle: 0.3885),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(start: 0.0, end: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child: Text(
                              'Username',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontSize: 14,
                                color: const Color(0x40040202),
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 135.0, middle: 0.5425),
                      Pin(size: 28.0, middle: 0.4993),
                      child: Text(
                        'Contact Info',
                        style: TextStyle(
                          fontFamily: 'Arial',
                          fontSize: 25,
                          color: const Color(0xff040202),
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 203.0, start: 5.5),
                      Pin(size: 63.0, middle: 0.5769),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(size: 94.0, end: 0.0),
                            Pin(size: 16.0, middle: 0.4894),
                            child: Text(
                              'Phone Number',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontSize: 14,
                                color: const Color(0x40040202),
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 65.0, start: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child:
                                // Adobe XD layer: 'iphone-computer-ico…' (shape)
                                Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 227.0, start: 5.5),
                      Pin(size: 63.0, middle: 0.6809),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(size: 118.0, end: 0.0),
                            Pin(size: 16.0, middle: 0.4894),
                            child: Text(
                              'WhatsApp Number',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontSize: 14,
                                color: const Color(0x40040202),
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 65.0, start: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child:
                                // Adobe XD layer: 'iphone-computer-ico…' (shape)
                                Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(),
                    Pinned.fromPins(
                      Pin(size: 279.0, end: 0.0),
                      Pin(size: 1.0, middle: 0.625),
                    ),
                    Pinned.fromPins(
                      Pin(size: 279.0, end: 0.0),
                      Pin(size: 1.0, middle: 0.7245),
                    ),
                    Pinned.fromPins(
                      Pin(start: 5.5, end: 0.0),
                      Pin(size: 63.0, end: -68.0),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(size: 225.0, start: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child: Stack(
                              children: <Widget>[
                                Pinned.fromPins(
                                  Pin(size: 116.0, end: 0.0),
                                  Pin(size: 16.0, middle: 0.4894),
                                  child: Text(
                                    'Website (Optional)',
                                    style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontSize: 14,
                                      color: const Color(0x40040202),
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Pinned.fromPins(
                                  Pin(size: 65.0, start: 0.0),
                                  Pin(start: 0.0, end: 0.0),
                                  child:
                                      // Adobe XD layer: 'iphone-computer-ico…' (shape)
                                      Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(36.0),
                                      image: DecorationImage(
                                        image: const AssetImage(''),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 279.0, end: 0.0),
                            Pin(size: 1.0, middle: 0.7823),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 236.0, start: 5.5),
                      Pin(size: 63.0, middle: 0.7849),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(size: 127.0, end: 0.0),
                            Pin(size: 16.0, middle: 0.4894),
                            child: Text(
                              'Facebook (Optional)',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontSize: 14,
                                color: const Color(0x40040202),
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 65.0, start: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child:
                                // Adobe XD layer: 'iphone-computer-ico…' (shape)
                                Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 216.0, start: 5.5),
                      Pin(size: 63.0, end: 78.0),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(size: 107.0, end: 0.0),
                            Pin(size: 16.0, middle: 0.4894),
                            child: Text(
                              'Twitter (Optional)',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontSize: 14,
                                color: const Color(0x40040202),
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 65.0, start: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child:
                                // Adobe XD layer: 'iphone-computer-ico…' (shape)
                                Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 237.0, start: 5.5),
                      Pin(size: 63.0, end: 5.0),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(size: 128.0, end: 0.0),
                            Pin(size: 16.0, middle: 0.4894),
                            child: Text(
                              'Instagram (Optional)',
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontSize: 14,
                                color: const Color(0x40040202),
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Pinned.fromPins(
                            Pin(size: 65.0, start: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child:
                                // Adobe XD layer: 'iphone-computer-ico…' (shape)
                                Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 279.0, end: 0.0),
                      Pin(size: 1.0, end: 135.5),
                      child: SvgPicture.string(
                        _svg_9hj4wk,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 279.0, end: 0.0),
                      Pin(size: 1.0, end: 64.5),
                      child: SvgPicture.string(
                        _svg_jt38rk,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 279.0, end: 0.0),
                      Pin(size: 1.0, end: -9.5),
                      child: SvgPicture.string(
                        _svg_3rtlkg,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 279.0, end: 0.0),
                      Pin(size: 1.0, middle: 0.4483),
                      child: SvgPicture.string(
                        _svg_wbizfw,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 87.0, middle: 0.5358),
                      Pin(size: 87.0, end: -211.0),
                      child: Stack(
                        children: <Widget>[
                          Pinned.fromPins(
                            Pin(start: 0.0, end: 0.0),
                            Pin(start: 0.0, end: 0.0),
                            child:
                                // Adobe XD layer: 'Add' (group)
                                SvgPicture.string(
                              _svg_m7jz7y,
                              allowDrawingOutsideViewBox: true,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Container(),
                        ],
                      ),
                    ),
                    Pinned.fromPins(
                      Pin(size: 1.0, start: 0.0),
                      Pin(size: 523.0, end: -287.5),
                      child: SvgPicture.string(
                        _svg_l93byv,
                        allowDrawingOutsideViewBox: true,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}


   