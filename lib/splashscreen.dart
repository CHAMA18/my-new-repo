import 'package:flutter/material.dart';

class SplashScreen {
  SplashScreen(
      {Color loaderColor,
      int seconds,
      double photoSize,
      dynamic onClick,
      dynamic navigateAfterSeconds,
      Text title,
      Color backgroundColor,
      TextStyle styleTextUnderTheLoader,
      Image image,
      Text loadingText,
      ImageProvider<dynamic> imageBackground,
      Gradient gradientBackground});
}
